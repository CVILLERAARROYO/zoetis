var app = new Vue({
    el: '#app',
    data: {
        // grupos: [],
        date: '',
        namecompany: '',
        name: '',
        email: '',
        // edad: '',
        grupo: [{
            aves: '',
            granja: '',
            edad: '',
            raza: '',
            plan: '',
            cama: '',
            bursometro: '',
            pesoAve: '',
            pesoBolsa: '',
            pesoBazo: '',
            simpleValue: '',
            BolsaBazo: '',
            MorfoBursal: '',
        }],

        loading: false

    },
    methods: {
        capurar() {

        },
        agregar() {



            var nuevoGrupo = {
                aves: '',
                granja: '',
                edad: '',
                raza: '',
                plan: '',
                cama: '',
                bursometro: '',
                pesoAve: '',
                pesoBolsa: '',
                pesoBazo: '',
                // simpleValue: '',
                BolsaBazo: '',
                MorfoBursal: '',
            }
            this.grupo.push(nuevoGrupo);
        },
        calcular() {

            for (let index = 0; index < app.grupo.length; index++) {
                const element = app.grupo[index];
                //console.log(element);

                element.BolsaBazo = element.pesoBolsa / element.pesoBazo;
                element.MorfoBursal = ((element.pesoBolsa / element.pesoAve) * 1000);
            }
        },
        Enviar() {
            if (this.email.trim() === '') {
                Swal.fire({
                    icon: 'warning',
                    text: 'Debe introducir una dirrección de correo para enviar los datos',
                    timer: 5000
                });
                return;
            }
            //multiples correos donde se enviaran
            let emails = ['camila.reyes@zoetis.com','adriana.i.leon@zoetis.com']
            emails.push(app.email);
            // console.log(emails);
            app.loading = true;
            var dataCalc = "";

            for (let index = 0; index < app.grupo.length; index++) {
                const element = app.grupo[index];
                dataCalc += `<b>Grupo: ${index + 1}</b><br>
                            Aves: ${element.aves} <br>
                            Granja: ${element.granja} <br>
                            Edad: ${element.edad} <br>
                            Raza: ${element.raza} <br>
                            Plan: ${element.plan} <br>
                            Cama: ${element.cama} <br>
                            Bursometro: ${element.bursometro} <br>
                            Peso ave: ${element.pesoAve} <br>
                            Peso bolsa: ${element.pesoBolsa} <br>
                            Peso bazo: ${element.pesoBazo} <br>
                            Relación bolsa-bazo: ${element.BolsaBazo} <br>
                            Indice morfometrico bursal: ${element.MorfoBursal} <br>
                            
                <br><br> ` ;
            }

            Email.send({
                Host: "smtp.gmail.com",
                Username: "poultryzoetis@gmail.com",
                Password: "amsmajsrtvudppuc",
                To: emails,
                From: "poultryzoetis@gmail.com",
                Subject: "HERRAMIENTA CÁLCULO | ZOETIS",
                Body: `Hola ${app.name} <br>
                        Gracias por usar nuestra Herramienta de cálculo <br>
                        Fecha de uso: ${app.date} <br>
                        Empresa: ${app.namecompany} <br><br>
                        TUS DATOS: <br><hr>
                        ${dataCalc}

                ` ,

            }).then(function () {
                if (message => 'OK') {
                    Swal.fire({
                        icon: 'success',
                        text: 'Muchas gracias sus datos han sido enviados',
                        timer: 2500
                    }).then(function () {
                        app.loading = false;
                    })
                }
            }
            );
            setTimeout(function () {
                location.reload();
            }, 7500);




        },



    },

})
